import React, {useEffect, useReducer} from 'react';
import useSocket from './useSocket';
import {Text} from 'react-native-elements';
import {StatusBar} from 'expo-status-bar';
import {StyleSheet, View} from 'react-native';
import {Match, MatchDispatch} from './types';

const INITIAL_STATE: Match = {
    homePoints: 0,
    matchId: '',
    outPoints: 0
}

const matchReducer = (state: Match, action: MatchDispatch) => {
    switch (action.type) {
        case 'GOAL_SCORED':
            return {...state, outPoints: action.payload.outPoints, homePoints: action.payload.homePoints};
        case 'MATCH_ENDED':
            return {...state, matchId: ''};
        case 'MATCH_STARTED':
            return {homePoints: 0, outPoints: 0, matchId: action.payload};
        default:
            return state;
    }
}

const MatchScreen = () => {
    const [state, dispatch] = useReducer(matchReducer, INITIAL_STATE);
    const {openSocket} = useSocket;

    useEffect(() => {
        openSocket(dispatch);
    }, []);

    return (
        <View style={styles.container}>
            <Text>Match id: {state.matchId ? state.matchId : 'No ongoing match!'}</Text>
            {
                state.matchId ?
                    <Text h3 style={{marginTop: 30}}>Current scoring: {state.homePoints} - {state.outPoints}</Text>
                    : null
            }
            <StatusBar style="auto"/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default MatchScreen;
