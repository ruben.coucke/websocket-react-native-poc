export interface Match {
    homePoints: number;
    outPoints: number;
    matchId: string;
}

export const MATCH_STARTED = 'MATCH_STARTED';
export const MATCH_ENDED = 'MATCH_ENDED';
export const GOAL_SCORED = 'GOAL_SCORED';

export interface MatchStartedDispatch {
    type: typeof MATCH_STARTED;
    payload: string;
}

export interface MatchEndedDispatch {
    type: typeof MATCH_ENDED;
}

export interface GoalScoredDispatch {
    type: typeof GOAL_SCORED;
    payload: {
        homePoints: number;
        outPoints: number;
    };
}

export type MatchDispatch = MatchEndedDispatch | MatchStartedDispatch | GoalScoredDispatch;
