import {Dispatch} from 'react';

let socket: WebSocket;
let dispatch: Dispatch<any>;

const openSocket = (socketDispatch: Dispatch<any>): WebSocket => {
    dispatch = socketDispatch;

    socket = new WebSocket("ws://192.168.0.131:3000");

    socket.onopen = () => {
        console.log('opened socket');
    }

    socket.onerror = (error) => {
        console.log(error);
    }

    socket.onclose = () => {
        console.log('socket closed');
    }

    socket.onmessage = ((event: MessageEvent) => {
        dispatch(JSON.parse(event.data));
    });

    return socket;
}

const closeSocket = () => {
    if (socket) {
        socket.close();
    }
}

const reopenSocket = () => {
    if (dispatch && socket.readyState === socket.CLOSED) {
        openSocket(dispatch);
    }
}

export default {openSocket, closeSocket, reopenSocket};
