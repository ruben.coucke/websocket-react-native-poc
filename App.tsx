import React, {useEffect} from 'react';
import MatchScreen from './src/MatchScreen';
import {AppState, AppStateStatus} from 'react-native';
import useSocket from './src/useSocket';

const {openSocket, closeSocket, reopenSocket} = useSocket;
const handleAppStateChange = (nextAppState: AppStateStatus) => {
    if (nextAppState === 'active') {
        reopenSocket();
    }
    if (nextAppState === 'background') {
        closeSocket();
    }
}

export default function App() {
    useEffect(() => {
        AppState.addEventListener('change', handleAppStateChange);
        return () => {
            AppState.removeEventListener('change', handleAppStateChange);
        };
    }, []);

    return <MatchScreen/>
}
